package com.example.task03;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class Task03Main {

    public static void main(String[] args) {
        Stream<Integer> intStream = Stream.of(null, 0,2,5);

        findMinMax(
                intStream,
                Integer::compareTo,
                (min, max) ->
                        System.out.println("min: " + min + " / max: " + max)
        );

    }

    public static <T> void findMinMax(
            Stream<? extends T> stream,
            Comparator<? super T> order,
            BiConsumer<? super T, ? super T> minMaxConsumer
    ) {
        AtomicReference<T> min = new AtomicReference<>();
        AtomicReference<T> max = new AtomicReference<>();

        AtomicReference<Boolean> wasNull = new AtomicReference<>(false);

        stream.forEach(obj -> {
            if (obj != null) {
                if (min.get() == null || order.compare(min.get(), obj) > 0) {
                    min.set(obj);
                }
                if (max.get() == null || order.compare(max.get(), obj) < 0) {
                    max.set(obj);
                }
            } else {
                if (min.get() == null) {
                    min.set(obj);
                    wasNull.set(true);
                }
                if (max.get() == null) {
                    max.set(obj);
                }
            }
        });

        if (wasNull.get())
            min.set(null);

        minMaxConsumer.accept(min.get(), max.get());
    }
}

