package com.example.task05;

import lombok.NonNull;

final public class MailMessage implements Post<String> {
  private String from;
  private String to;
  private String content;

  public MailMessage(@NonNull String from, @NonNull String to, @NonNull String content) {
    this.from = from;
    this.to = to;
    this.content = content;
  }

  @Override
  public String getFrom() {
    return from;
  }

  @Override
  public String getTo() {
    return to;
  }

  @Override
  public String getContent() {
    return content;
  }
}
