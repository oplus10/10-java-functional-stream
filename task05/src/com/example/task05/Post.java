package com.example.task05;

public interface Post<T> {
  public String getFrom();

  public String getTo();

  public T getContent();
}
