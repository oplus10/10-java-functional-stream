package com.example.task05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import lombok.NonNull;

public class MailService<T> implements Consumer<Post<T>> {
  private ArrayList<Post<T>> posts;

  public MailService() {
    this.posts = new ArrayList<>();
  }

  public Map<String, List<T>> getMailBox() {
    Map<String, List<T>> mailBox = new HashMap<>();
    for (var post : posts) {
      mailBox.compute(post.getTo(), (to, mails) -> {
        if (mails == null) {
          return new ArrayList<T>(List.of(post.getContent()));
        }
        mails.add(post.getContent());
        return mails;
      });
    }
    return mailBox;
  }

  @Override
  public void accept(@NonNull Post<T> post) {
    posts.add(post);
  }
}
