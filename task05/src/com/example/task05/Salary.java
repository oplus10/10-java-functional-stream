package com.example.task05;

import lombok.NonNull;

final public class Salary implements Post<Integer> {
  private String from;
  private String to;
  private Integer content;

  public Salary(@NonNull String from, @NonNull String to, @NonNull Integer content) {
    this.from = from;
    this.to = to;
    this.content = content;
  }

  @Override
  public String getFrom() {
    return from;
  }

  @Override
  public String getTo() {
    return to;
  }

  @Override
  public Integer getContent() {
    return content;
  }
}
