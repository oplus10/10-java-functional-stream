package com.example.task02;

import java.util.stream.IntStream;
import java.util.Iterator;

public class Task02Main {

    public static void main(String[] args) {
        cycleGrayCode(16)
                .limit(10)
                .forEach(System.out::println);
    }

    public static IntStream cycleGrayCode(int n) throws IllegalArgumentException {
        if (n <= 0 || n > 16)
            throw new IllegalArgumentException();

        return IntStream.iterate(0, i -> (i + 1) % (1 << n))
                .map(i -> i ^ (i >> 1))
                .map(i -> i % (1 << n));
    }


}